﻿using Antlr4.Runtime;
using Metaf.Parser;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metaf.Parser.Tests.Lib {
    internal static class LexerHelpers {
        public static void AssertSingleCommentAfterPosition(int tokenIndex, CommonTokenStream tokens, string expectedValue = null) {
            var commentTokens = tokens.GetHiddenTokensToRight(tokenIndex, Antlr4.Runtime.Lexer.Hidden);
            Assert.AreEqual(1, commentTokens?.Count);
            Assert.AreEqual(MetaFLexer.COMMENT, commentTokens[0].Type);

            if (expectedValue != null) {
                Assert.AreEqual(expectedValue, commentTokens[0].Text.Trim());
            }
        }

        public static void AssertTokenAtPosition(int expectedToken, int tokenIndex, CommonTokenStream tokens, string expectedValue = null) {
            var actualToken = tokens.Get(tokenIndex);
            Assert.AreEqual(expectedToken, tokens.Get(tokenIndex).Type, $"Expected {MetaFLexer.tokenNames[expectedToken]}:{expectedToken}. Actual {MetaFLexer.tokenNames[actualToken.Type]}:{actualToken.Type}. (Text: {actualToken.Text})");

            if (expectedValue != null) {
                Assert.AreEqual(expectedValue, $"{tokens.GetText(actualToken, actualToken)}");
            }
        }
    }
}
