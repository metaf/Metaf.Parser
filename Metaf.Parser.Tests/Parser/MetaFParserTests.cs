﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Antlr4.Runtime;
using Metaf.Parser;

namespace Metaf.Parser.Tests.Parser {

    [TestClass]
    public class MetaFParserTests {
        private MetaFParser SetupParser(string input) {
            var lexer = new MetaFLexer(new AntlrInputStream(input));
            var tokens = new CommonTokenStream(lexer);
            return new MetaFParser(tokens);
        }

        [TestMethod]
        public void TestValidInput() {
            var input = @"STATE: {Default}";

            var parser = SetupParser(input);
            var tree = parser.meta();

            Assert.IsNotNull(tree);
        }
    }
}
