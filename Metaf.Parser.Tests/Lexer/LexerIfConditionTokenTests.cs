﻿using Metaf.Parser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Metaf.Parser.Tests.Lib.LexerHelpers;
using static Metaf.Parser.Tests.Lexer.MetaFLexerTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Metaf.Parser.Tests.Lexer {
    [TestClass]
    public class LexerIfConditionTokenTests {
        [TestMethod]
        public void TestIfCondAllTokenization() {
            var tokens = Setup("All");
            AssertTokenAtPosition(MetaFLexer.IF_COND_ALL, 0, tokens);
        }

        [TestMethod]
        public void TestIfCondAlwaysTokenization() {
            var tokens = Setup("Always");
            AssertTokenAtPosition(MetaFLexer.IF_COND_ALWAYS, 0, tokens);
        }

        [TestMethod]
        public void TestIfCondAnyTokenization() {
            var tokens = Setup("Any");
            AssertTokenAtPosition(MetaFLexer.IF_COND_ANY, 0, tokens);
        }

        [TestMethod]
        public void TestIfCondBlockETokenization() {
            var tokens = Setup("BlockE");
            AssertTokenAtPosition(MetaFLexer.IF_COND_BLOCK_E, 0, tokens);
        }

        [TestMethod]
        public void TestIfCondBuPercentGETokenization() {
            var tokens = Setup("BuPercentGE");
            AssertTokenAtPosition(MetaFLexer.IF_COND_BU_PERCENT_GE, 0, tokens);
        }

        [TestMethod]
        public void TestIfCondCellETokenization() {
            var tokens = Setup("CellE");
            AssertTokenAtPosition(MetaFLexer.IF_COND_CELL_E, 0, tokens);
        }

        [TestMethod]
        public void TestIfCondChatCaptureTokenization() {
            var tokens = Setup("ChatCapture");
            AssertTokenAtPosition(MetaFLexer.IF_COND_CHAT_CAPTURE, 0, tokens);
        }

        [TestMethod]
        public void TestIfCondChatMatchTokenization() {
            var tokens = Setup("ChatMatch");
            AssertTokenAtPosition(MetaFLexer.IF_COND_CHAT_MATCH, 0, tokens);
        }

        [TestMethod]
        public void TestIfCondDeathTokenization() {
            var tokens = Setup("Death");
            AssertTokenAtPosition(MetaFLexer.IF_COND_DEATH, 0, tokens);
        }

        [TestMethod]
        public void TestIfCondDistToRteGETokenization() {
            var tokens = Setup("DistToRteGE");
            AssertTokenAtPosition(MetaFLexer.IF_COND_DIST_TO_RTE_GE, 0, tokens);
        }

        [TestMethod]
        public void TestIfCondExitPortalTokenization() {
            var tokens = Setup("ExitPortal");
            AssertTokenAtPosition(MetaFLexer.IF_COND_EXIT_PORTAL, 0, tokens);
        }

        [TestMethod]
        public void TestIfCondExprTokenization() {
            var tokens = Setup("Expr");
            AssertTokenAtPosition(MetaFLexer.IF_COND_EXPR, 0, tokens);
        }

        [TestMethod]
        public void TestIfCondIntoPortalTokenization() {
            var tokens = Setup("IntoPortal");
            AssertTokenAtPosition(MetaFLexer.IF_COND_INTO_PORTAL, 0, tokens);
        }

        [TestMethod]
        public void TestIfCondItemCountGETokenization() {
            var tokens = Setup("ItemCountGE");
            AssertTokenAtPosition(MetaFLexer.IF_COND_ITEM_COUNT_GE, 0, tokens);
        }

        [TestMethod]
        public void TestIfCondItemCountLETokenization() {
            var tokens = Setup("ItemCountLE");
            AssertTokenAtPosition(MetaFLexer.IF_COND_ITEM_COUNT_LE, 0, tokens);
        }

        [TestMethod]
        public void TestIfCondMainSlotsLETokenization() {
            var tokens = Setup("MainSlotsLE");
            AssertTokenAtPosition(MetaFLexer.IF_COND_MAIN_SLOTS_LE, 0, tokens);
        }

        [TestMethod]
        public void TestIfCondMobsInDistNameTokenization() {
            var tokens = Setup("MobsInDist_Name");
            AssertTokenAtPosition(MetaFLexer.IF_COND_MOBS_IN_DIST_NAME, 0, tokens);
        }

        [TestMethod]
        public void TestIfCondMobsInDistPriorityTokenization() {
            var tokens = Setup("MobsInDist_Priority");
            AssertTokenAtPosition(MetaFLexer.IF_COND_MOBS_IN_DIST_PRIORITY, 0, tokens);
        }

        [TestMethod]
        public void TestIfCondNavEmptyTokenization() {
            var tokens = Setup("NavEmpty");
            AssertTokenAtPosition(MetaFLexer.IF_COND_NAV_EMPTY, 0, tokens);
        }

        [TestMethod]
        public void TestIfCondNeedToBuffTokenization() {
            var tokens = Setup("NeedToBuff");
            AssertTokenAtPosition(MetaFLexer.IF_COND_NEED_TO_BUFF, 0, tokens);
        }

        [TestMethod]
        public void TestIfCondNeverTokenization() {
            var tokens = Setup("Never");
            AssertTokenAtPosition(MetaFLexer.IF_COND_NEVER, 0, tokens);
        }

        [TestMethod]
        public void TestIfCondNoMobsInDistTokenization() {
            var tokens = Setup("NoMobsInDist");
            AssertTokenAtPosition(MetaFLexer.IF_COND_NO_MOBS_IN_DIST, 0, tokens);
        }

        [TestMethod]
        public void TestIfCondNotTokenization() {
            var tokens = Setup("Not");
            AssertTokenAtPosition(MetaFLexer.IF_COND_NOT, 0, tokens);
        }

        [TestMethod]
        public void TestIfCondPSecondsInStateGETokenization() {
            var tokens = Setup("PSecsInStateGE");
            AssertTokenAtPosition(MetaFLexer.IF_COND_P_SECS_IN_STATE_GE, 0, tokens);
        }

        [TestMethod]
        public void TestIfCondSecondsInStateGETokenization() {
            var tokens = Setup("SecsInStateGE");
            AssertTokenAtPosition(MetaFLexer.IF_COND_SECS_IN_STATE_GE, 0, tokens);
        }

        [TestMethod]
        public void TestIfCondVendorClosedTokenization() {
            var tokens = Setup("VendorClosed");
            AssertTokenAtPosition(MetaFLexer.IF_COND_VENDOR_CLOSED, 0, tokens);
        }

        [TestMethod]
        public void TestIfCondVendorOpenTokenization() {
            var tokens = Setup("VendorOpen");
            AssertTokenAtPosition(MetaFLexer.IF_COND_VENDOR_OPEN, 0, tokens);
        }
    }
}
