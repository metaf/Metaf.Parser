﻿using Metaf.Parser;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Metaf.Parser.Tests.Lib.LexerHelpers;
using static Metaf.Parser.Tests.Lexer.MetaFLexerTests;

namespace Metaf.Parser.Tests.Lexer {
    [TestClass]
    public class LexerActionTokenTests {
        [TestMethod]
        public void TestActionSetStateTokenization() {
            var tokens = Setup("SetState");
            AssertTokenAtPosition(MetaFLexer.ACTION_SET_STATE, 0, tokens);
        }

        [TestMethod]
        public void TestActionChatTokenization() {
            var tokens = Setup("Chat");
            AssertTokenAtPosition(MetaFLexer.ACTION_CHAT, 0, tokens);
        }

        [TestMethod]
        public void TestActionDoAllTokenization() {
            var tokens = Setup("DoAll");
            AssertTokenAtPosition(MetaFLexer.ACTION_DO_ALL, 0, tokens);
        }

        [TestMethod]
        public void TestActionEmbedNavTokenization() {
            var tokens = Setup("EmbedNav");
            AssertTokenAtPosition(MetaFLexer.ACTION_EMBED_NAV, 0, tokens);
        }

        [TestMethod]
        public void TestActionCallStateTokenization() {
            var tokens = Setup("CallState");
            AssertTokenAtPosition(MetaFLexer.ACTION_CALL_STATE, 0, tokens);
        }

        [TestMethod]
        public void TestActionReturnTokenization() {
            var tokens = Setup("Return");
            AssertTokenAtPosition(MetaFLexer.ACTION_RETURN, 0, tokens);
        }

        [TestMethod]
        public void TestActionDoExprTokenization() {
            var tokens = Setup("DoExpr");
            AssertTokenAtPosition(MetaFLexer.ACTION_DO_EXPR, 0, tokens);
        }

        [TestMethod]
        public void TestActionChatExprTokenization() {
            var tokens = Setup("ChatExpr");
            AssertTokenAtPosition(MetaFLexer.ACTION_CHAT_EXPR, 0, tokens);
        }

        [TestMethod]
        public void TestActionSetWatchdogTokenization() {
            var tokens = Setup("SetWatchdog");
            AssertTokenAtPosition(MetaFLexer.ACTION_SET_WATCHDOG, 0, tokens);
        }

        [TestMethod]
        public void TestActionClearWatchdogTokenization() {
            var tokens = Setup("ClearWatchdog");
            AssertTokenAtPosition(MetaFLexer.ACTION_CLEAR_WATCHDOG, 0, tokens);
        }

        [TestMethod]
        public void TestActionGetOptTokenization() {
            var tokens = Setup("GetOpt");
            AssertTokenAtPosition(MetaFLexer.ACTION_GET_OPT, 0, tokens);
        }

        [TestMethod]
        public void TestActionSetOptTokenization() {
            var tokens = Setup("SetOpt");
            AssertTokenAtPosition(MetaFLexer.ACTION_SET_OPT, 0, tokens);
        }

        [TestMethod]
        public void TestActionCreateViewTokenization() {
            var tokens = Setup("CreateView");
            AssertTokenAtPosition(MetaFLexer.ACTION_CREATE_VIEW, 0, tokens);
        }

        [TestMethod]
        public void TestActionDestroyViewTokenization() {
            var tokens = Setup("DestroyView");
            AssertTokenAtPosition(MetaFLexer.ACTION_DESTROY_VIEW, 0, tokens);
        }

        [TestMethod]
        public void TestActionDestroyAllViewsTokenization() {
            var tokens = Setup("DestroyAllViews");
            AssertTokenAtPosition(MetaFLexer.ACTION_DESTROY_ALL_VIEWS, 0, tokens);
        }

        [TestMethod]
        public void TestActionNoneTokenization() {
            var tokens = Setup("None");
            AssertTokenAtPosition(MetaFLexer.ACTION_NONE, 0, tokens);
        }
    }
}
