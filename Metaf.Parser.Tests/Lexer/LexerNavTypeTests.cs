﻿using Metaf.Parser;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using static Metaf.Parser.Tests.Lib.LexerHelpers;
using static Metaf.Parser.Tests.Lexer.MetaFLexerTests;

namespace Metaf.Parser.Tests.Lexer {
    [TestClass]
    public class LexerNavTypeTests {

        [TestMethod]
        public void TestNavTypeCirularTokenization() {
            string input = "circular";
            var tokens = Setup(input);

            AssertTokenAtPosition(MetaFLexer.NAV_TYPE_CIRCULAR, 0, tokens);
        }

        [TestMethod]
        public void TestNavTypeLinearTokenization() {
            string input = "linear";
            var tokens = Setup(input);

            AssertTokenAtPosition(MetaFLexer.NAV_TYPE_LINEAR, 0, tokens);
        }

        [TestMethod]
        public void TestNavTypeOnceTokenization() {
            string input = "once";
            var tokens = Setup(input);

            AssertTokenAtPosition(MetaFLexer.NAV_TYPE_ONCE, 0, tokens);
        }

        [TestMethod]
        public void TestNavTypeFollowTokenization() {
            string input = "follow";
            var tokens = Setup(input);

            AssertTokenAtPosition(MetaFLexer.NAV_TYPE_FOLLOW, 0, tokens);
        }
    }
}
