using Antlr4.Runtime;
using Metaf.Parser;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using static Metaf.Parser.Tests.Lib.LexerHelpers;

namespace Metaf.Parser.Tests.Lexer
{
    [TestClass]
    public class MetaFLexerTests {
        public static CommonTokenStream? Setup(string input) {
            var lexer = new MetaFLexer(new AntlrInputStream(input));
            var tokens = new CommonTokenStream(lexer);

            tokens.Fill();

            return tokens;
        }

        [TestMethod]
        public void TestIdentifierTokenization() {
            string input = "myVariable123";
            var tokens = Setup(input);

            AssertTokenAtPosition(MetaFLexer.IDENTIFIER, 0, tokens, "myVariable123");
        }

        [TestMethod]
        public void TestIntegerTokenization() {
            string input = "123";
            var tokens = Setup(input);

            AssertTokenAtPosition(MetaFLexer.NUMBER, 0, tokens, "123");
        }

        [TestMethod]
        public void TestNegativeIntegerTokenization() {
            string input = "-456";
            var tokens = Setup(input);

            AssertTokenAtPosition(MetaFLexer.DECIMAL, 0, tokens, "-456");
        }

        [TestMethod]
        public void TestDecimalTokenization() {
            string input = "789.012";
            var tokens = Setup(input);

            AssertTokenAtPosition(MetaFLexer.DECIMAL, 0, tokens, "789.012");
        }

        [TestMethod]
        public void TestNegativeDecimalTokenization() {
            string input = "-512.012";
            var tokens = Setup(input);

            AssertTokenAtPosition(MetaFLexer.DECIMAL, 0, tokens, "-512.012");
        }

        [TestMethod]
        public void TestPrefixedHexadecimalTokenization() {
            string input = "0x1A3a4234";
            var tokens = Setup(input);

            AssertTokenAtPosition(MetaFLexer.HEX, 0, tokens, "0x1A3a4234");
        }

        [TestMethod]
        public void TestHexadecimalTokenization() {
            string input = "1A3a4234";
            var tokens = Setup(input);

            AssertTokenAtPosition(MetaFLexer.HEX, 0, tokens, "1A3a4234");
        }

        [TestMethod]
        public void TestBlockCommentTokenization() {
            string input = "/~ This is a block comment ~/";
            var tokens = Setup(input);

            AssertTokenAtPosition(MetaFLexer.COMMENT, 0, tokens, "/~ This is a block comment ~/");
        }

        [TestMethod]
        public void TestMultlineBlockCommentTokenization() {
            string input = "/~ This is a block comment\n spanning multiple lines ~/";
            var tokens = Setup(input);

            AssertTokenAtPosition(MetaFLexer.COMMENT, 0, tokens, "/~ This is a block comment\n spanning multiple lines ~/");
        }

        [TestMethod]
        public void TestCommentLineTokenization() {
            string input = "~~ Comment at the end of the line";
            var tokens = Setup(input);


            AssertTokenAtPosition(MetaFLexer.COMMENT, 0, tokens, "~~ Comment at the end of the line");
        }

        [TestMethod]
        public void TestMultipleCurlyStringsTokenization() {
            string input = "{nextstate} {more}";
            var tokens = Setup(input);

            AssertTokenAtPosition(MetaFLexer.CURLYCONTENTS, 0, tokens, "nextstate");
            AssertTokenAtPosition(MetaFLexer.CURLYCONTENTS, 1, tokens, "more");
        }

        [TestMethod]
        public void TestCurlyStringTokenizationWithSpaces() {
            string input = "{test  state}";
            var tokens = Setup(input);

            AssertTokenAtPosition(MetaFLexer.CURLYCONTENTS, 0, tokens, "test  state");
        }

        [TestMethod]
        public void TestCurlyStringTokenizationWithEscapedCurlies() {
            string input = "{test\\}state} {\\{} {\\}}";
            var tokens = Setup(input);

            AssertTokenAtPosition(MetaFLexer.CURLYCONTENTS, 0, tokens, "test}state");
            AssertTokenAtPosition(MetaFLexer.CURLYCONTENTS, 1, tokens, "{");
            AssertTokenAtPosition(MetaFLexer.CURLYCONTENTS, 2, tokens, "}");
        }

        [TestMethod]
        public void TestDoubleEscapedSingleLineCurlies() {
            string input = "{test{{this}}that}";
            var tokens = Setup(input);

            AssertTokenAtPosition(MetaFLexer.CURLYCONTENTS, 0, tokens, "test{this}that");
        }

        [TestMethod]
        public void TestIndendations() {
            string input = 
@"
IF:
    IF:
    IF:
IF:
";
            var tokens = Setup(input);

            AssertTokenAtPosition(MetaFLexer.IF_LABEL, 0, tokens);
            AssertTokenAtPosition(MetaFLexer.INDENT, 1, tokens);
            AssertTokenAtPosition(MetaFLexer.IF_LABEL, 2, tokens);
            AssertTokenAtPosition(MetaFLexer.NL, 3, tokens);
            AssertTokenAtPosition(MetaFLexer.IF_LABEL, 4, tokens);
            AssertTokenAtPosition(MetaFLexer.NL, 5, tokens);
            AssertTokenAtPosition(MetaFLexer.DEDENT, 6, tokens);
            AssertTokenAtPosition(MetaFLexer.IF_LABEL, 7, tokens);
        }
    }
}