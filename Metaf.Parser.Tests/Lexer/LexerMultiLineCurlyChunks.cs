﻿using Metaf.Parser;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using static Metaf.Parser.Tests.Lib.LexerHelpers;
using static Metaf.Parser.Tests.Lexer.MetaFLexerTests;

namespace Metaf.Parser.Tests.Lexer {
    [TestClass]
    public class LexerMultiLineCurlyChunks {

        [TestMethod]
        public void TestMultiLineExpressionBlock() {
            string input = "{\nthis[];\nthat[];\n}";
            var tokens = Setup(input);

            AssertTokenAtPosition(MetaFLexer.CURLYCONTENTS, 0, tokens, "\nthis[];\nthat[];\n");
        }

        [TestMethod]
        public void TestMultiLineXMLBlock() {
            string input = "{\n<?xml ?>\n<test>\n</xml>\n}";
            var tokens = Setup(input);

            AssertTokenAtPosition(MetaFLexer.CURLYCONTENTS, 0, tokens, "\n<?xml ?>\n<test>\n</xml>\n");
        }
    }
}
