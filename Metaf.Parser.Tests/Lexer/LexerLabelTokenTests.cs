﻿using Metaf.Parser;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using static Metaf.Parser.Tests.Lib.LexerHelpers;
using static Metaf.Parser.Tests.Lexer.MetaFLexerTests;

namespace Metaf.Parser.Tests.Lexer {
    [TestClass]
    public class LexerLabelTokenTests {

        [TestMethod]
        public void TestStateLabelTokenization() {
            string input = "STATE:";
            var tokens = Setup(input);

            AssertTokenAtPosition(MetaFLexer.STATE_LABEL, 0, tokens);
        }

        [TestMethod]
        public void TestIfLabelTokenization() {
            string input = "IF:";
            var tokens = Setup(input);

            AssertTokenAtPosition(MetaFLexer.IF_LABEL, 0, tokens);
        }

        [TestMethod]
        public void TestDoLabelTokenization() {
            string input = "DO:";
            var tokens = Setup(input);

            AssertTokenAtPosition(MetaFLexer.DO_LABEL, 0, tokens);
        }

        [TestMethod]
        public void TestNavLabelTokenization() {
            string input = "NAV:";
            var tokens = Setup(input);

            AssertTokenAtPosition(MetaFLexer.NAV_LABEL, 0, tokens);
        }
    }
}
