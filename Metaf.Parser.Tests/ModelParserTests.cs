﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Metaf.Parser;
using Metaf.Parser.Models;
using Metaf.Parser.Models.Conditions;
using Metaf.Parser.Models.Actions;
using System.IO;

namespace Metaf.Parser.Tests {
    [TestClass]
    public class ModelParserTests {

        private Meta Setup(string afContents) {
            var parser = new ModelParser(afContents);
            var parseResult = parser.TryParse(out Meta? meta);

            if (!parseResult) {
                foreach (var error in parser.ErrorHandler.Errors) {
                    Console.WriteLine($"Parse Error: {error.ErrorText} @ {error.Line}:{error.Column} ({error.Exception?.Message})");
                }
            }

            Assert.IsTrue(parseResult, "Parse result was false");
            Assert.IsNotNull(meta, "Meta was null");

            return meta;
        }

        [TestMethod]
        public void CanParseBasicAF() {
            var meta = Setup("STATE: {Default}\n\tIF: Never\n\t\tDO: None");

            Assert.AreEqual(1, meta.States.Count);
            Assert.AreEqual("Default", meta.States.First().Name);
            Assert.AreEqual(1, meta.States.First().Rules.Count);
            Assert.IsInstanceOfType<NeverCondition>(meta.States.First().Rules.First().Condition);
            Assert.IsInstanceOfType<NoneAction>(meta.States.First().Rules.First().Action);
        }

        [TestMethod]
        public void CanParseAllExamples() {
            var exampleFiles = Directory.GetFiles("./../../../../examples/", "*.af");
            foreach (var exampleFile in exampleFiles) {
                Console.WriteLine($"Parsing file: {exampleFile}");
                var exampleContents = File.ReadAllText(exampleFile);
                var meta = Setup(exampleContents);
            }
        }
    }
}
