# Metaf Parser

MetaF Parser is a csharp dotnet standard 2.0 library that utilizes ANTLR4 grammars to parse scripts written in the Metaf language to an easy to work with model format.

## Features

- **ANTLR4 Grammar:** The project includes a comprehensive ANTLR4 grammar for the Metaf language, defining the syntax and structure of the scripts.

- **C# ANTLR Visitor:** A C# visitor class (`MetafModelBuilderVisitor`) is implemented to traverse the parsed AST (Abstract Syntax Tree) and construct a corresponding `Meta` model.

- **C# Model Parser:** A C# class (`ModelParser`) with a simplified interface for turning meta file contents into a `Meta` model.

- **Meta Model:** The `Meta` model represents the structured information extracted from Metaf scripts, including states, rules, conditions, actions, nav routes, and more.

## Usage

Install the `Metaf.Parser` nuget package and see the examples below.

## Example

```csharp
string afContents = "..."; // Your Metaf script here
var parser = new ModelParser(afContents);
var parseResult = parser.TryParse(out Meta? meta);

if (!parseResult) {
	// check parser.ErrorHandler.Errors for a list of parser errors.
}
else {
	// Now you can use the 'meta' object to access the parsed information
	foreach(var state in meta.States) {
		// do stuff
	}
}
```

## Updating the grammar

To update the grammar, update the lexer/parser .g4 grammar files and rebuild the project. The updated parser / lexer code should be automatically generated.

## Contributions

Contributions are welcome! If you find any issues, have suggestions, or want to improve the project, feel free to submit a pull request or open an issue on the GitLab repository.
License


This project is licensed under the MIT License.