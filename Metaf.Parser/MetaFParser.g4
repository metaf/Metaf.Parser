parser grammar MetaFParser;

options {
    tokenVocab = MetaFLexer;
}

meta: (state_block | NL)* (nav_block | NL)* EOF ;

state_block: STATE_LABEL name=CURLYCONTENTS INDENT (if_block)+ DEDENT ;
if_block: IF_LABEL condition_block INDENT do_block DEDENT ;

condition_block
   : cond=IF_COND_ALL (INDENT (condition_block | NL)+ DEDENT)? NL?
   | cond=IF_COND_ALWAYS NL?
   | cond=IF_COND_ANY (INDENT (condition_block | NL)+ DEDENT)? NL?
   | cond=IF_COND_BLOCK_E landblock=HEX NL?
   | cond=IF_COND_BU_PERCENT_GE percent=NUMBER NL?
   | cond=IF_COND_CELL_E landcell=HEX NL?
   | cond=IF_COND_CHAT_CAPTURE pattern=CURLYCONTENTS (OPENCURL chat_color_list CLOSECURL | chat_color_list_curly=CURLYCONTENTS) NL?
   | cond=IF_COND_CHAT_MATCH pattern=CURLYCONTENTS NL?
   | cond=IF_COND_DEATH NL?
   | cond=IF_COND_DIST_TO_RTE_GE distance=(NUMBER | DECIMAL) NL?
   | cond=IF_COND_EXIT_PORTAL NL?
   | cond=IF_COND_EXPR expression=CURLYCONTENTS NL?
   | cond=IF_COND_INTO_PORTAL NL?
   | cond=IF_COND_ITEM_COUNT_GE count=NUMBER name=CURLYCONTENTS NL?
   | cond=IF_COND_ITEM_COUNT_LE count=NUMBER name=CURLYCONTENTS NL?
   | cond=IF_COND_MAIN_SLOTS_LE count=NUMBER NL?
   | cond=IF_COND_MOBS_IN_DIST_NAME count=NUMBER distance=(NUMBER | DECIMAL) pattern=CURLYCONTENTS NL?
   | cond=IF_COND_MOBS_IN_DIST_PRIORITY count=NUMBER distance=(NUMBER | DECIMAL) priority=NUMBER
   | cond=IF_COND_NAV_EMPTY NL?
   | cond=IF_COND_NEED_TO_BUFF NL?
   | cond=IF_COND_NEVER NL?
   | cond=IF_COND_NO_MOBS_IN_DIST distance=(NUMBER | DECIMAL) NL?
   | cond=IF_COND_NOT condition_block NL?
   | cond=IF_COND_P_SECS_IN_STATE_GE seconds=NUMBER NL?
   | cond=IF_COND_SECS_IN_STATE_GE seconds=NUMBER NL?
   | cond=IF_COND_VENDOR_CLOSED NL?
   | cond=IF_COND_VENDOR_OPEN NL?
;

chat_color_list: (NUMBER SEMICOLON?)* ;

do_block: DO_LABEL (action_block | do_all_block) ;
do_all_block: ACTION_DO_ALL INDENT (action_block | do_all_block)+ NL? DEDENT;

action_block: (
    action=ACTION_CALL_STATE state=CURLYCONTENTS return=CURLYCONTENTS NL
    | action=ACTION_CHAT text=CURLYCONTENTS NL
    | action=ACTION_CHAT_EXPR expression=CURLYCONTENTS NL
    | action=ACTION_DO_EXPR expression=CURLYCONTENTS NL
    | action=ACTION_EMBED_NAV id=IDENTIFIER name=CURLYCONTENTS nav_transform? NL
    | action=ACTION_NONE NL
    | action=ACTION_RETURN NL
    | action=ACTION_SET_STATE name=CURLYCONTENTS NL
    | action=ACTION_SET_WATCHDOG distance=(NUMBER | DECIMAL) seconds=(NUMBER | DECIMAL) state=CURLYCONTENTS NL
    | action=ACTION_CLEAR_WATCHDOG NL
    | action=ACTION_GET_OPT option=CURLYCONTENTS variable=CURLYCONTENTS NL
    | action=ACTION_SET_OPT option=CURLYCONTENTS expression=CURLYCONTENTS NL
    | action=ACTION_CREATE_VIEW id=CURLYCONTENTS xml=CURLYCONTENTS NL
    | action=ACTION_DESTROY_VIEW id=CURLYCONTENTS NL
    | action=ACTION_DESTROY_ALL_VIEWS NL
);

nav_transform: OPENCURL a=(NUMBER | DECIMAL) b=(NUMBER | DECIMAL) c=(NUMBER | DECIMAL) d=(NUMBER | DECIMAL) e=(NUMBER | DECIMAL) f=(NUMBER | DECIMAL) g=(NUMBER | DECIMAL) CLOSECURL;

nav_block: NAV_LABEL name=IDENTIFIER type=(
    NAV_TYPE_CIRCULAR
    | NAV_TYPE_LINEAR
    | NAV_TYPE_ONCE
    | NAV_TYPE_FOLLOW
) (INDENT navline* DEDENT | NL) ;

navline
    : type=NAV_CHT pos=navcoord text=CURLYCONTENTS NL?
    | type=NAV_PNT pos=navcoord NL?
    | type=NAV_PAU pos=navcoord milliseconds=NUMBER NL?
    | type=NAV_TLK pos=navcoord tpos=navcoord objectclass=NUMBER name=CURLYCONTENTS NL?
    | type=NAV_RCL pos=navcoord name=CURLYCONTENTS NL?
    | type=NAV_PTL pos=navcoord tpos=navcoord objectclass=NUMBER name=CURLYCONTENTS NL?
    | type=NAV_VND pos=navcoord id=(HEX | NUMBER) name=CURLYCONTENTS NL?
    | type=NAV_CHK pos=navcoord NL?
    | type=NAV_PRT pos=navcoord id=NUMBER NL?
    | type=NAV_FLW id=(HEX | NUMBER) name=CURLYCONTENTS NL?
    | type=NAV_JMP pos=navcoord heading=(NUMBER | DECIMAL) walk=CURLYCONTENTS milliseconds=(NUMBER | DECIMAL) NL?
; 

navcoord: x=(DECIMAL | NUMBER) y=(DECIMAL | NUMBER) z=(DECIMAL | NUMBER) ;
