﻿using Antlr4.Runtime;
using Antlr4.Runtime.Misc;
using Antlr4.Runtime.Tree;
using Metaf.Parser;
using Metaf.Parser.Lib;
using Metaf.Parser.Models;
using Metaf.Parser.Models.Actions;
using Metaf.Parser.Models.Conditions;
using Metaf.Parser.Models.NavNodes;
using System;
using System.Linq;
using static System.Net.Mime.MediaTypeNames;

namespace Metaf.Parser.Visitors {
    public class MetaFModelBuilderVisitor : MetaFParserBaseVisitor<object> {
        public MetaFLexer Lexer { get; }
        public CommonTokenStream TokenStream { get; }
        public MetaFParser Parser { get; }
        public AntlrInputStream InputStream { get; }

        /// <summary>
        /// The error handler used for lexing / parsing
        /// </summary>
        public MetafErrorListener ErrorHandler { get; private set; }

        /// <summary>
        /// The string contents of the af file being parsed.
        /// </summary>
        public string AFContents { get; }

        /// <summary>
        /// Creates a new visitor 
        /// </summary>
        /// <param name="contents"></param>
        public MetaFModelBuilderVisitor(string contents) {
            AFContents = contents;

            InputStream = new AntlrInputStream(AFContents);
            Lexer = new MetaFLexer(InputStream);
            TokenStream = new CommonTokenStream(Lexer);
            Parser = new MetaFParser(TokenStream);

            Lexer.RemoveErrorListeners();
            Parser.RemoveErrorListeners();

            ErrorHandler = new MetafErrorListener(this);
            Lexer.AddErrorListener(ErrorHandler);
            Parser.AddErrorListener(ErrorHandler);
        }

        public Meta? Build() {
            return VisitMeta(Parser.meta()) as Meta;
        }

        public override object VisitMeta([NotNull] MetaFParser.MetaContext context) {
            var meta = new Meta();

            foreach (var stateContext in context.state_block()) {
                meta.States.Add(VisitState(stateContext));
            }

            foreach (var navRouteContext in context.nav_block()) {
                meta.NavRoutes.Add(VisitNavRoute(navRouteContext));
            }

            return meta;
        }

        public State VisitState([NotNull] MetaFParser.State_blockContext context) {
            var state = new State(context.name.Text);

            foreach (var ruleContext in context.if_block()) {
                state.Rules.Add(VisitRule(ruleContext));
            }

            return state;
        }

        public Rule VisitRule([NotNull] MetaFParser.If_blockContext context) {
            var rule = new Rule(VisitCondition(context.condition_block()), VisitAction(context.do_block()));
            return rule;
        }

        private ConditionBase VisitCondition(MetaFParser.Condition_blockContext context) {
            switch (context.cond.Type) {
                case MetaFLexer.IF_COND_ALL:
                    return new AllCondition() {
                        Children = context.condition_block().Select(ctx => VisitCondition(ctx)).ToList()
                    };
                case MetaFLexer.IF_COND_ALWAYS:
                    return new AlwaysCondition() { };
                case MetaFLexer.IF_COND_ANY:
                    return new AnyCondition() {
                        Children = context.condition_block().Select(ctx => VisitCondition(ctx)).ToList()
                    };
                case MetaFLexer.IF_COND_BLOCK_E:
                    return new BlockECondition() {
                        Landblock = TokenToInt(context, context.landblock)
                    };
                case MetaFLexer.IF_COND_BU_PERCENT_GE:
                    return new BuPercentGECondition() {
                        Burden = TokenToInt(context, context.percent)
                    };
                case MetaFLexer.IF_COND_CELL_E:
                    return new CellECondition() {
                        Landcell = TokenToInt(context, context.landcell)
                    };
                case MetaFLexer.IF_COND_CHAT_CAPTURE:
                    return new ChatCaptureCondition() {
                        Pattern = context.pattern.Text,
                        ColorIds = context.chat_color_list()?.GetText() ?? context.chat_color_list_curly?.Text ?? ""
                    };
                case MetaFLexer.IF_COND_CHAT_MATCH:
                    return new ChatMatchCondition() {
                        Pattern = context.pattern.Text
                    };
                case MetaFLexer.IF_COND_DEATH:
                    return new DeathCondition();
                case MetaFLexer.IF_COND_DIST_TO_RTE_GE:
                    return new DistToRteGECondition() {
                        Distance = TokenToDouble(context, context.distance)
                    };
                case MetaFLexer.IF_COND_EXIT_PORTAL:
                    return new ExitPortalCondition();
                case MetaFLexer.IF_COND_EXPR:
                    return new ExprCondition() {
                        Expression = context.expression.Text
                    };
                case MetaFLexer.IF_COND_INTO_PORTAL:
                    return new IntoPortalCondition();
                case MetaFLexer.IF_COND_ITEM_COUNT_GE:
                    return new ItemCountGECondition() {
                        Name = context.name.Text,
                        Count = TokenToInt(context, context.count)
                    };
                case MetaFLexer.IF_COND_ITEM_COUNT_LE:
                    return new ItemCountLECondition() {
                        Name = context.name.Text,
                        Count = TokenToInt(context, context.count)
                    };
                case MetaFLexer.IF_COND_MAIN_SLOTS_LE:
                    return new MainSlotsLECondition() {
                        Count = TokenToInt(context, context.count)
                    };
                case MetaFLexer.IF_COND_MOBS_IN_DIST_NAME:
                    return new MobsInDist_NameCondition() {
                        Pattern = context.pattern.Text,
                        Count = TokenToInt(context, context.count),
                        Distance = TokenToDouble(context, context.distance)
                    };
                case MetaFLexer.IF_COND_MOBS_IN_DIST_PRIORITY:
                    return new MobsInDist_PriorityCondition() {
                        Priority = TokenToInt(context, context.priority),
                        Count = TokenToInt(context, context.count),
                        Distance = TokenToDouble(context, context.distance)
                    };
                case MetaFLexer.IF_COND_NAV_EMPTY:
                    return new NavEmptyCondition();
                case MetaFLexer.IF_COND_NEED_TO_BUFF:
                    return new NeedToBuffCondition();
                case MetaFLexer.IF_COND_NEVER:
                    return new NeverCondition();
                case MetaFLexer.IF_COND_NOT:
                    return new NotCondition() {
                        Condition = VisitCondition(context.condition_block().FirstOrDefault())
                    };
                case MetaFLexer.IF_COND_NO_MOBS_IN_DIST:
                    return new NoMobsInDistCondition() {
                        Distance = TokenToDouble(context, context.distance)
                    };
                case MetaFLexer.IF_COND_P_SECS_IN_STATE_GE:
                    return new PSecsInStateGECondition() {
                        Seconds = TokenToInt(context, context.seconds)
                    };
                case MetaFLexer.IF_COND_SECS_IN_STATE_GE:
                    return new SecsInStateGECondition() {
                        Seconds = TokenToInt(context, context.seconds)
                    };
                case MetaFLexer.IF_COND_VENDOR_CLOSED:
                    return new VendorClosedCondition();
                case MetaFLexer.IF_COND_VENDOR_OPEN:
                    return new VendorOpenCondition();
                default:
                    throw new ArgumentException($"Unknown condition type: {MetaFLexer.DefaultVocabulary.GetSymbolicName(context.cond.Type)}");
            }
        }

        private ActionBase VisitAction(MetaFParser.Do_blockContext context) {
            var actionBlock = context.action_block();
            var doAllBlock = context.do_all_block();

            if (actionBlock != null) {
                return VisitActionBlock(context.action_block());
            }
            else if (doAllBlock != null) {
                return VisitDoAllActionBlock(context.do_all_block());
            }
            else {
                throw new Exception($"Do block is missing an action!");
            }
        }

        private ActionBase VisitDoAllActionBlock(MetaFParser.Do_all_blockContext context) {
            return new DoAllAction() {
                Children = context.children.Select(child => {
                    if (child is MetaFParser.Do_all_blockContext doAllCtx) {
                        return VisitDoAllActionBlock(doAllCtx);
                    }
                    else if (child is MetaFParser.Action_blockContext actionCtx) {
                        return VisitActionBlock(actionCtx);
                    }
                    return null;
                }).Where(c => c != null).Cast<ActionBase>().ToList()
            };
        }

        private ActionBase VisitActionBlock(MetaFParser.Action_blockContext context) {
            switch (context.action.Type) {
                case MetaFLexer.ACTION_CALL_STATE:
                    return new CallStateAction() {
                        NewState = context.state.Text,
                        ReturnState = context.@return.Text
                    };
                case MetaFLexer.ACTION_CHAT:
                    return new ChatAction() {
                        Message = context.text.Text
                    };
                case MetaFLexer.ACTION_CHAT_EXPR:
                    return new ChatExprAction() {
                        Expression = context.expression.Text
                    };
                case MetaFLexer.ACTION_CLEAR_WATCHDOG:
                    return new ClearWatchdogAction();
                case MetaFLexer.ACTION_CREATE_VIEW:
                    return new CreateViewAction() {
                        Id = context.id.Text,
                        XML = context.xml.Text
                    };
                case MetaFLexer.ACTION_DESTROY_ALL_VIEWS:
                    return new DestroyAllViewsAction();
                case MetaFLexer.ACTION_DESTROY_VIEW:
                    return new DestroyViewAction() {
                        Id = context.id.Text,
                    };
                case MetaFLexer.ACTION_DO_EXPR:
                    return new DoExprAction() {
                        Expression = context.expression.Text
                    };
                case MetaFLexer.ACTION_EMBED_NAV:
                    return new EmbedNavAction() {
                        Id = context.id.Text,
                        Name = context.name?.Text ?? "",
                        Transform = context.nav_transform() == null ? null : new EmbedNavAction.NavTransform() {
                            A = TokenToDouble(context, context.nav_transform().a),
                            B = TokenToDouble(context, context.nav_transform().b),
                            C = TokenToDouble(context, context.nav_transform().c),
                            D = TokenToDouble(context, context.nav_transform().d),
                            E = TokenToDouble(context, context.nav_transform().e),
                            F = TokenToDouble(context, context.nav_transform().f),
                            G = TokenToDouble(context, context.nav_transform().g),
                        }
                    };
                case MetaFLexer.ACTION_GET_OPT:
                    return new GetOptAction() {
                        SettingName = context.option.Text,
                        VariableName = context.variable.Text
                    };
                case MetaFLexer.ACTION_NONE:
                    return new NoneAction();
                case MetaFLexer.ACTION_RETURN:
                    return new ReturnAction();
                case MetaFLexer.ACTION_SET_OPT:
                    return new SetOptAction() {
                        SettingName = context.option.Text,
                        Expression = context.expression.Text
                    };
                case MetaFLexer.ACTION_SET_STATE:
                    return new SetStateAction() {
                        Name = context.name.Text,
                    };
                case MetaFLexer.ACTION_SET_WATCHDOG:
                    return new SetWatchdogAction() {
                        Distance = TokenToDouble(context, context.distance),
                        TransferState = context.state.Text,
                        Seconds = TokenToDouble(context, context.seconds)
                    };
                default:
                    throw new ArgumentException($"Unknown action type: {MetaFLexer.DefaultVocabulary.GetSymbolicName(context.action.Type)}");
            }
        }

        public NavRoute VisitNavRoute([NotNull] MetaFParser.Nav_blockContext context) {
            var navRoute = new NavRoute(context.IDENTIFIER().GetText());

            // Set the type of nav route
            navRoute.Type = GetNavRouteType(context, context.type.Type);

            foreach (var navline in context.navline()) {
                navRoute.Nodes.Add(VisitNavLine(navline));
            }

            return navRoute;
        }

        private NavNodeBase VisitNavLine(MetaFParser.NavlineContext context) {
            switch (context.type.Type) {
                case MetaFLexer.NAV_CHK:
                    return new CheckpointNavNode() {
                        X = TokenToDouble(context, context.pos.x),
                        Y = TokenToDouble(context, context.pos.y),
                        Z = TokenToDouble(context, context.pos.z)
                    };
                case MetaFLexer.NAV_CHT:
                    return new ChatNavNode() {
                        X = TokenToDouble(context, context.pos.x),
                        Y = TokenToDouble(context, context.pos.y),
                        Z = TokenToDouble(context, context.pos.z),
                        Text = context.text.Text
                    };
                case MetaFLexer.NAV_FLW:
                    return new FollowNavNode() {
                        Id = TokenToInt(context, context.id),
                        Name = context.name.Text,
                    };
                case MetaFLexer.NAV_JMP:
                    return new JumpNavNode() {
                        X = TokenToDouble(context, context.pos.x),
                        Y = TokenToDouble(context, context.pos.y),
                        Z = TokenToDouble(context, context.pos.z),
                        Heading = TokenToDouble(context, context.heading),
                        Power = TokenToDouble(context, context.milliseconds),
                        Walk = TokenToBool(context, context.walk)
                    };
                case MetaFLexer.NAV_PAU:
                    return new PauseNavNode() {
                        X = TokenToDouble(context, context.pos.x),
                        Y = TokenToDouble(context, context.pos.y),
                        Z = TokenToDouble(context, context.pos.z),
                        Milliseconds = TokenToInt(context, context.milliseconds),
                    };
                case MetaFLexer.NAV_PNT:
                    return new PointNavNode() {
                        X = TokenToDouble(context, context.pos.x),
                        Y = TokenToDouble(context, context.pos.y),
                        Z = TokenToDouble(context, context.pos.z),
                    };
                case MetaFLexer.NAV_PRT:
                    throw new InvalidOperationException("NavNode type PRT is deprecated!");
                case MetaFLexer.NAV_PTL:
                    return new UsePortalNPCNavNode() {
                        X = TokenToDouble(context, context.pos.x),
                        Y = TokenToDouble(context, context.pos.y),
                        Z = TokenToDouble(context, context.pos.z),
                        ObjectClass = TokenToInt(context, context.objectclass),
                        Name = context.name?.Text ?? "",
                        TargetX = TokenToDouble(context, context.tpos.x),
                        TargetY = TokenToDouble(context, context.tpos.y),
                        TargetZ = TokenToDouble(context, context.tpos.z),
                    };
                case MetaFLexer.NAV_RCL:
                    return new RecallNavNode() {
                        X = TokenToDouble(context, context.pos.x),
                        Y = TokenToDouble(context, context.pos.y),
                        Z = TokenToDouble(context, context.pos.z),
                        SpellName = context.name.Text,
                    };
                case MetaFLexer.NAV_TLK:
                    return new TalkToNPCNavNode() {
                        X = TokenToDouble(context, context.pos.x),
                        Y = TokenToDouble(context, context.pos.y),
                        Z = TokenToDouble(context, context.pos.z),
                        ObjectClass = TokenToInt(context, context.objectclass),
                        Name = context.name.Text,
                        TargetX = TokenToDouble(context, context.tpos.x),
                        TargetY = TokenToDouble(context, context.tpos.y),
                        TargetZ = TokenToDouble(context, context.tpos.z),
                    };
                case MetaFLexer.NAV_VND:
                    return new OpenVendorNavNode() {
                        X = TokenToDouble(context, context.pos.x),
                        Y = TokenToDouble(context, context.pos.y),
                        Z = TokenToDouble(context, context.pos.z),
                        Id = TokenToInt(context, context.id),
                        Name = context.name.Text
                    };
                default:
                    throw new InvalidOperationException($"Unknown nav node type: {MetaFLexer.DefaultVocabulary.GetSymbolicName(context.type.Type)}!");
            }
        }

        // Helper method to convert nav route type string to NavRouteType enum
        private NavRouteType GetNavRouteType(MetaFParser.Nav_blockContext context, int tokenType) {
            switch (tokenType) {
                case MetaFLexer.NAV_TYPE_CIRCULAR:
                    return NavRouteType.Circular;
                case MetaFLexer.NAV_TYPE_FOLLOW:
                    return NavRouteType.Follow;
                case MetaFLexer.NAV_TYPE_LINEAR:
                    return NavRouteType.Linear;
                case MetaFLexer.NAV_TYPE_ONCE:
                    return NavRouteType.Once;
                default:
                    // Handle unknown type or throw an exception
                    throw new InvalidOperationException($"Unknown NavRouteType: {MetaFLexer.DefaultVocabulary.GetSymbolicName(tokenType)}");
            }
        }

        private int TokenToInt(ParserRuleContext context, IToken token) {
            switch (token.Type) {
                case MetaFLexer.NUMBER:
                    if (!int.TryParse(token.Text, out int intResult)) {
                        throw new InvalidCastException($"Unable to cast {token.Text} to an integer!");
                    }
                    return intResult;
                case MetaFLexer.HEX:
                    if (token.Text.Trim().StartsWith("0x")) {
                        return Convert.ToInt32(token.Text, 16);
                    }
                    else {
                        if (!int.TryParse(token.Text, System.Globalization.NumberStyles.HexNumber, null, out int hexResult)) {
                            throw new InvalidCastException($"Unable to parse {token.Text} hex string as an integer!");
                        }
                        return hexResult;
                    }
            }

            throw new InvalidCastException($"Unable to cast token of type {MetaFLexer.DefaultVocabulary.GetSymbolicName(token.Type)} to an integer!");
        }

        private double TokenToDouble(ParserRuleContext context, IToken token) {
            switch (token.Type) {
                case MetaFLexer.DECIMAL:
                    if (!double.TryParse(token.Text, out double doubleResult)) {
                        ErrorHandler.ReportError(Parser, new RecognitionException($"Unable to cast {token.Text} to a double!", Lexer, InputStream, context));
                    }
                    return doubleResult;
                case MetaFLexer.NUMBER:
                case MetaFLexer.HEX:
                    return TokenToInt(context, token);
            }

            ErrorHandler.ReportError(Parser, new RecognitionException($"Unable to cast token of type {MetaFLexer.DefaultVocabulary.GetSymbolicName(token.Type)} to a double!", Lexer, InputStream, context));

            return 0;
        }

        private bool TokenToBool(ParserRuleContext context, IToken token) {
            switch (token.Type) {
                case MetaFLexer.CURLYCONTENTS:
                    return token.Text.Trim().ToLower().Equals("true");
            }

            throw new InvalidCastException($"Unable to cast token of type {MetaFLexer.DefaultVocabulary.GetSymbolicName(token.Type)} to a boolean!");
        }
    }
}