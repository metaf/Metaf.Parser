﻿namespace Metaf.Parser.Models {
    public abstract class ActionBase {
        /// <summary>
        /// The type of action
        /// </summary>
        public ActionType Type { get; }

        public ActionBase(ActionType type) {
            Type = type;
        }
    }
}