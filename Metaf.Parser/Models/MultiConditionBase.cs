﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Metaf.Parser.Models {
    /// <summary>
    /// A condition that contains child conditions, like Any/All.
    /// </summary>
    public abstract class MultiConditionBase : ConditionBase {
        /// <summary>
        /// A list of all child conditions
        /// </summary>
        public List<ConditionBase> Children { get; set;  } = new List<ConditionBase>();

        public MultiConditionBase(ConditionType type) : base(type) {
            
        }
    }
}
