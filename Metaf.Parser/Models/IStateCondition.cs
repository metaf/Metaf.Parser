﻿namespace Metaf.Parser.Models {
    public class StateCondition {
        public ConditionType Type { get; }

        public StateCondition(ConditionType type) {
            Type = type;
        }
    }
}