﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Metaf.Parser.Models.Conditions {
    /// <summary>
    /// A condition that evaluates to true if the time in seconds spent in this state is greater than or equal to
    /// <see cref="SecsInStateGECondition.Count"/>.
    /// </summary>
    public class SecsInStateGECondition : ConditionBase {
        /// <summary>
        /// The amount of seconds spent in this state to check against.
        /// </summary>
        public int Seconds { get; set; } = 0;

        public SecsInStateGECondition() : base(ConditionType.SecsInStateGE) {

        }
    }
}
