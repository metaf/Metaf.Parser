﻿namespace Metaf.Parser.Models.Conditions {
    /// <summary>
    /// A condition that evaluates to true if the time in seconds since meta entered this state is greater than or equal to 
    /// <see cref="PSecsInStateGECondition.Seconds"/>. The time continues accruing even if the meta is stopped.
    /// </summary>
    public class PSecsInStateGECondition : ConditionBase {
        /// <summary>
        /// The number of seconds to check against
        /// </summary>
        public int Seconds { get; set; }

        public PSecsInStateGECondition() : base(ConditionType.PSecsInStateGE) {

        }
    }
}
