﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Metaf.Parser.Models.Conditions {
    /// <summary>
    /// A condition that never evaluates to true.
    /// </summary>
    public class NeverCondition : ConditionBase {
        public NeverCondition() : base(ConditionType.Never) {

        }
    }
}
