﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Metaf.Parser.Models.Conditions {
    /// <summary>
    /// A condition that evaluates to true if your main backpack free item slot count is less than or equal to
    /// <see cref="MainSlotsLECondition.Count"/>.
    /// </summary>
    public class MainSlotsLECondition : ConditionBase {
        /// <summary>
        /// The free main backpack item slot count to check against.
        /// </summary>
        public int Count { get; set; } = 0;

        public MainSlotsLECondition() : base(ConditionType.MainSlotsLE) {

        }
    }
}
