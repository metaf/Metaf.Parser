﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Metaf.Parser.Models.Conditions {
    /// <summary>
    /// A condition that evaluates to true if your character needs to refresh their buffs.
    /// </summary>
    public class NeedToBuffCondition : ConditionBase {
        public NeedToBuffCondition() : base(ConditionType.NeedToBuff) {

        }
    }
}
