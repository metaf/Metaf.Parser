﻿namespace Metaf.Parser.Models.Conditions {
    /// <summary>
    /// A condition that evaluates to true if the expression <see cref="ExprCondition.Expression"/> evaluates 
    /// to true.
    /// </summary>
    public class ExprCondition : ConditionBase {
        /// <summary>
        /// The expression to check against
        /// </summary>
        public string Expression { get; set; }

        public ExprCondition() : base(ConditionType.Expr) {

        }
    }
}
