﻿namespace Metaf.Parser.Models.Conditions {
    /// <summary>
    /// A condition that evaluates to true if the number of your items in your inventory exactly matching the name
    /// <see cref="ItemCountGECondition.Name"/> is greater than or equal to <see cref="ItemCountGECondition.Count"/>.
    /// </summary>
    public class ItemCountGECondition : ConditionBase {
        /// <summary>
        /// The exact name of the item to check.
        /// </summary>
        public string Name { get; set; } = "";

        /// <summary>
        /// The number of items to trigger against
        /// </summary>
        public int Count { get; set; } = 0;

        public ItemCountGECondition() : base(ConditionType.ItemCountGE) {

        }
    }
}
