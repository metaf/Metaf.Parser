﻿namespace Metaf.Parser.Models.Conditions {
    /// <summary>
    /// A condition that evaluates to true if the number monsters with a priority exactly matching <see cref="MobsInDist_PriorityCondition.Priority"/>,
    /// within the distance of <see cref="MobsInDist_PriorityCondition.Distance"/>, is greater than or equal to
    /// <see cref="MobsInDist_PriorityCondition.Count"/>.
    /// </summary>
    public class MobsInDist_PriorityCondition : ConditionBase {
        /// <summary>
        /// The monster priority (from the vtank monsters tab) to check. This is an exact match.
        /// </summary>
        public int Priority { get; set; } = 0;

        /// <summary>
        /// The distance to check that the matching monsters are within.
        /// </summary>
        public double Distance { get; set; } = 0;

        /// <summary>
        /// The number of monsters to trigger against
        /// </summary>
        public int Count { get; set; } = 0;

        public MobsInDist_PriorityCondition() : base(ConditionType.MobsInDist_Priority) {

        }
    }
}
