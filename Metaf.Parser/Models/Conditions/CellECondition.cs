﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Metaf.Parser.Models.Conditions {
    /// <summary>
    /// A condition that evaluates to true if yout current landcell is equal to <see cref="CellECondition.Landcell"/>.
    /// </summary>
    public class CellECondition : ConditionBase {
        /// <summary>
        /// The landblock to match
        /// </summary>
        public int Landcell { get; set; } = 0;

        public CellECondition() : base(ConditionType.CellE) {

        }
    }
}
