﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Metaf.Parser.Models.Conditions {
    /// <summary>
    /// A condition that evaluates to true if your character has died while in this state.
    /// </summary>
    public class DeathCondition : ConditionBase {
        public DeathCondition() : base(ConditionType.Death) {

        }
    }
}
