﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Metaf.Parser.Models.Conditions {
    /// <summary>
    /// A condition that evaluates to true if a vendor window has been opened while this state is active.
    /// </summary>
    public class VendorOpenCondition : ConditionBase {
        public VendorOpenCondition() : base(ConditionType.VendorOpen) {

        }
    }
}
