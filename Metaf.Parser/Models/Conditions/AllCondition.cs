﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Metaf.Parser.Models.Conditions {
    /// <summary>
    /// A condition that evaluates to true if all children evaluate to true.
    /// </summary>
    public class AllCondition : MultiConditionBase {
        public AllCondition() : base(ConditionType.All) {

        }
    }
}
