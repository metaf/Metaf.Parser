﻿namespace Metaf.Parser.Models.Conditions {
    /// <summary>
    /// A condition that evaluates to true if the number monsters with a name matching the regex <see cref="MobsInDist_NameCondition.Pattern"/>,
    /// within the distance of <see cref="MobsInDist_NameCondition.Distance"/>, is greater than or equal to
    /// <see cref="MobsInDist_NameCondition.Count"/>.
    /// </summary>
    public class MobsInDist_NameCondition : ConditionBase {
        /// <summary>
        /// The regex pattern to match against the monster name
        /// </summary>
        public string Pattern { get; set; } = "";

        /// <summary>
        /// The distance to check that the matching monsters are within.
        /// </summary>
        public double Distance { get; set; } = 0;

        /// <summary>
        /// The number of monsters to trigger against
        /// </summary>
        public int Count { get; set; } = 0;

        public MobsInDist_NameCondition() : base(ConditionType.MobsInDist_Name) {

        }
    }
}
