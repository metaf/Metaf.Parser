﻿namespace Metaf.Parser.Models.Conditions {
    /// <summary>
    /// A condition that evaluates to true if <see cref="ChatCaptureCondition.MatchRegex"/> matches an incoming chat message and 
    /// <see cref="ChatCaptureCondition.ColorIds"/>. This will also set expression variables based on the defined capture groups.
    /// </summary>
    public class ChatCaptureCondition : ConditionBase {
        /// <summary>
        /// The regular expression to match incoming chat against
        /// </summary>
        public string Pattern { get; set; }

        /// <summary>
        /// A list of chat colors separated by colons to check against.
        /// </summary>
        public string ColorIds { get; set; } = "";

        public ChatCaptureCondition() : base(ConditionType.ChatCapture) {

        }
    }
}
