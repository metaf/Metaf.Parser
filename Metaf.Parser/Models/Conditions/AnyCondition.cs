﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Metaf.Parser.Models.Conditions {
    /// <summary>
    /// A condition that evaluates to true if any of the children evaluate to true.
    /// </summary>
    public class AnyCondition : MultiConditionBase {
        public AnyCondition() : base(ConditionType.Any) {

        }
    }
}
