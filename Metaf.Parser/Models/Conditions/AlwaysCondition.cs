﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Metaf.Parser.Models.Conditions {
    /// <summary>
    /// A condition that always evaluates to true.
    /// </summary>
    public class AlwaysCondition : ConditionBase {
        public AlwaysCondition() : base(ConditionType.Always) {

        }
    }
}
