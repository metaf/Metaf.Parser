﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Metaf.Parser.Models.Conditions {
    /// <summary>
    /// A condition that evaluates to true if the currently loaded vtank navroute is empty.
    /// </summary>
    public class NavEmptyCondition : ConditionBase {
        public NavEmptyCondition() : base(ConditionType.NavEmpty) {

        }
    }
}
