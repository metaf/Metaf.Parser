﻿namespace Metaf.Parser.Models.Conditions {
    /// <summary>
    /// A condition that evaluates to true if the time remaining in seconds on the specified <see cref="SecsOnSpellGECondition.SpellId"/> 
    /// is greater than or equal to <see cref="SecsOnSpellGECondition.Seconds"/>.
    /// </summary>
    public class SecsOnSpellGECondition : ConditionBase {
        /// <summary>
        /// The spell id to check against
        /// </summary>
        public int SpellId { get; set; }

        /// <summary>
        /// The number of seconds to check against
        /// </summary>
        public int Seconds { get; set; }

        public SecsOnSpellGECondition() : base(ConditionType.SecsOnSpellGE) {

        }
    }
}
