﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Metaf.Parser.Models.Conditions {
    /// <summary>
    /// A condition that evaluates to true if your character has exited a portal while in this state.
    /// </summary>
    public class ExitPortalCondition : ConditionBase {
        public ExitPortalCondition() : base(ConditionType.ExitPortal) {

        }
    }
}
