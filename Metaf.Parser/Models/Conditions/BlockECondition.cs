﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Metaf.Parser.Models.Conditions {
    /// <summary>
    /// A condition that evaluates to true if yout current landblock is equal to <see cref="BlockECondition.Landblock"/>.
    /// </summary>
    public class BlockECondition : ConditionBase {
        /// <summary>
        /// The landblock to match
        /// </summary>
        public int Landblock { get; set; } = 0;

        public BlockECondition() : base(ConditionType.BlockE) {

        }
    }
}
