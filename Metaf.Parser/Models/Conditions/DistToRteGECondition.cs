﻿namespace Metaf.Parser.Models.Conditions {
    /// <summary>
    /// A condition that evaluates to true the shortest distance to the currently loaded vtank navroute is greater than or 
    /// equal to <see cref="DistToRteGECondition.Distance"/>.
    /// </summary>
    public class DistToRteGECondition : ConditionBase {
        /// <summary>
        /// The distance to check against
        /// </summary>
        public double Distance { get; set; }

        public DistToRteGECondition() : base(ConditionType.DistToRteGE) {

        }
    }
}
