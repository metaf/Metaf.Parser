﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Metaf.Parser.Models.Conditions {
    /// <summary>
    /// A condition that evaluates to true if your character has entered a portal while in this state.
    /// </summary>
    public class IntoPortalCondition : ConditionBase {
        public IntoPortalCondition() : base(ConditionType.IntoPortal) {

        }
    }
}
