﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Metaf.Parser.Models.Conditions {
    /// <summary>
    /// A condition that evaluates to true if no monsters are within <see cref="NoMobsInDistCondition.NoMobsInDistCondition"/> 
    /// of your character.
    /// </summary>
    public class NoMobsInDistCondition : ConditionBase {
        /// <summary>
        /// The distance to check that monsters are within
        /// </summary>
        public double Distance { get; set; } = 0;

        public NoMobsInDistCondition() : base(ConditionType.NoMobsInDist) {

        }
    }
}
