﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Metaf.Parser.Models.Conditions {
    /// <summary>
    /// A condition that evaluates to true if child condition is false.
    /// </summary>
    public class NotCondition : ConditionBase {
        /// <summary>
        /// The condition to check
        /// </summary>
        public ConditionBase Condition { get; set; }

        public NotCondition() : base(ConditionType.Not) {

        }
    }
}
