﻿namespace Metaf.Parser.Models.Conditions {
    /// <summary>
    /// A condition that evaluates to true if your characters current burden percentage is greater than or equal to 
    /// <see cref="BuPercentGECondition.Burden"/>
    /// </summary>
    public class BuPercentGECondition : ConditionBase {
        /// <summary>
        /// The burden percentage to check against
        /// </summary>
        public int Burden { get; set; }

        public BuPercentGECondition() : base(ConditionType.BuPercentGE) {

        }
    }
}
