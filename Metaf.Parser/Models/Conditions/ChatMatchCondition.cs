﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Metaf.Parser.Models.Conditions {
    /// <summary>
    /// A condition that evaluates to true if <see cref="ChatMatchCondition.Pattern"/> matches an incoming chat message.
    /// </summary>
    public class ChatMatchCondition : ConditionBase {
        /// <summary>
        /// The regular expression string to check against each incoming chat message.
        /// </summary>
        public string Pattern { get; set; } = "";

        public ChatMatchCondition() : base(ConditionType.ChatMatch) {

        }
    }
}
