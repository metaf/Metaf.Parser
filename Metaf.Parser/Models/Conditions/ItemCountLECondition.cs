﻿namespace Metaf.Parser.Models.Conditions {
    /// <summary>
    /// A condition that evaluates to true if the number of your items in your inventory exactly matching the name
    /// <see cref="ItemCountLECondition.Name"/> is less than or equal to <see cref="ItemCountLECondition.Count"/>.
    /// </summary>
    public class ItemCountLECondition : ConditionBase {
        /// <summary>
        /// The exact name of the item to check.
        /// </summary>
        public string Name { get; set; } = "";

        /// <summary>
        /// The number of items to trigger against
        /// </summary>
        public int Count { get; set; } = 0;

        public ItemCountLECondition() : base(ConditionType.ItemCountLE) {

        }
    }
}
