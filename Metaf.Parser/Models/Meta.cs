﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metaf.Parser.Models {
    public class Meta {
        /// <summary>
        /// The name of this meta. Usually the filename.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// A list of available states
        /// </summary>
        public List<State> States { get; } = new List<State>();

        /// <summary>
        /// A list of embedded nav routes
        /// </summary>
        public List<NavRoute> NavRoutes { get; } = new List<NavRoute>();

        public Meta() {
        
        }
    }
}
