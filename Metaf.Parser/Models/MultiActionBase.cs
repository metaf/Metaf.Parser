﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Metaf.Parser.Models {
    /// <summary>
    /// An action containing other actions, like DoAll.
    /// </summary>
    public abstract class MultiActionBase : ActionBase {
        /// <summary>
        /// A list of child actions
        /// </summary>
        public List<ActionBase> Children { get; set; } = new List<ActionBase>();

        protected MultiActionBase(ActionType type) : base(type) {

        }
    }
}
