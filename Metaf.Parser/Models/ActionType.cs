﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Metaf.Parser.Models {
    public enum ActionType {
        None = 0,
        SetState = 1,
        Chat = 2,
        DoAll = 3,
        EmbedNav = 4,
        CallState = 5,
        Return = 6,
        DoExpr = 7,
        ChatExpr = 8,
        SetWatchdog = 9,
        ClearWatchdog = 10,
        GetOpt = 11,
        SetOpt = 12,
        CreateView = 13,
        DestroyView = 14,
        DestroyAllViews = 15
    }
}
