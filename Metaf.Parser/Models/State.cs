﻿using System.Collections.Generic;

namespace Metaf.Parser.Models {
    public class State {
        /// <summary>
        /// The name of this state
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// A list of rules, each containing a condition / action.
        /// </summary>
        public List<Rule> Rules { get; } = new List<Rule>();

        public State(string name) {
            Name = name;
        }
    }
}