﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Metaf.Parser.Models.NavNodes {
    /// <summary>
    /// A nav node that runs to the specified location, and then waits for a server response to verify that your character is
    /// at the specified location.
    /// </summary>
    public class CheckpointNavNode : PositionNavNode {
        public CheckpointNavNode() : base(NavNodeType.Checkpoint) {

        }
    }
}
