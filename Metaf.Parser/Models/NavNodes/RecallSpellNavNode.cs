﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Metaf.Parser.Models.NavNodes {
    /// <summary>
    /// A nav node that casts the specified recall spell
    /// </summary>
    public class RecallNavNode : PositionNavNode {
        /// <summary>
        /// The name of the recall spell to cast
        /// </summary>
        public string SpellName { get; set; } = "";

        public RecallNavNode() : base(NavNodeType.Recall) {

        }
    }
}
