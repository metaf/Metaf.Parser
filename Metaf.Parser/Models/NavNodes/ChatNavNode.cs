﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Metaf.Parser.Models.NavNodes {
    /// <summary>
    /// A nav node that sends the specified <see cref="Text"/> to the chat window.
    /// </summary>
    public class ChatNavNode : PositionNavNode {
        /// <summary>
        /// The text to send to the chat window.
        /// </summary>
        public string Text { get; set; }

        public ChatNavNode() : base(NavNodeType.Chat) {

        }
    }
}
