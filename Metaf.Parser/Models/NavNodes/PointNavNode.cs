﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Metaf.Parser.Models.NavNodes {
    /// <summary>
    /// A nav node that runs to the specified location.
    /// </summary>
    public class PointNavNode : PositionNavNode {
        public PointNavNode() : base(NavNodeType.Point) {

        }
    }
}
