﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Metaf.Parser.Models.NavNodes {
    /// <summary>
    /// A nav node that causes your character to perform a jump.
    /// </summary>
    public class JumpNavNode : PositionNavNode {
        /// <summary>
        /// The heading to face while jumping
        /// </summary>
        public double Heading { get; set; }

        /// <summary>
        /// Wether to perform a "walking" jump or not. (holding shift in the client is a walking jump)
        /// </summary>
        public bool Walk { get; set; } = false;

        /// <summary>
        /// The power of the jump, between 0-1000. This is how long in milliseconds the spacebar is being held down for in the client.
        /// Max is 1000 (1 second).
        /// </summary>
        public double Power { get; set; }

        public JumpNavNode() : base(NavNodeType.Jump) {

        }
    }
}
