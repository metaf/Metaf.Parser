﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Metaf.Parser.Models.NavNodes {
    /// <summary>
    /// A nav node that opens the specified vendor.
    /// </summary>
    public class OpenVendorNavNode : PositionNavNode {
        /// <summary>
        /// The id of the vendor npc to open.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The name of the vendor
        /// </summary>
        public string Name { get; set; } = "";

        public OpenVendorNavNode() : base(NavNodeType.OpenVendor) {

        }
    }
}
