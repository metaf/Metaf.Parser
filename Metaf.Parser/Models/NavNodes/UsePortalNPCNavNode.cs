﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Metaf.Parser.Models.NavNodes {
    /// <summary>
    /// A nav node that uses the specified portal / npc.
    /// </summary>
    public class UsePortalNPCNavNode : PositionNavNode {
        /// <summary>
        /// The object class of the object to be used.
        /// </summary>
        public int ObjectClass { get; set; }

        /// <summary>
        /// The name of the object to be used
        /// </summary>
        public string Name { get; set; } = "";

        public double TargetX { get; set; }
        public double TargetY { get; set; }
        public double TargetZ { get; set; }

        public UsePortalNPCNavNode() : base(NavNodeType.Portal_NPC) {

        }
    }
}
