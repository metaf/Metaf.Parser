﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Metaf.Parser.Models.NavNodes {
    /// <summary>
    /// A special type of NavNode that follows a character.
    /// </summary>
    public class FollowNavNode : NavNodeBase {
        public FollowNavNode() : base(NavNodeType.Follow) {

        }

        public int Id { get; internal set; }
        public string Name { get; internal set; }
    }
}
