﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Metaf.Parser.Models.NavNodes {
    /// <summary>
    /// A nav node that pauses for the specified <see cref="Milliseconds"/>
    /// </summary>
    public class PauseNavNode : PositionNavNode {
        /// <summary>
        /// The amount of time to pause in milliseconds
        /// </summary>
        public int Milliseconds { get; set; } = 0;

        public PauseNavNode() : base(NavNodeType.Pause) {

        }
    }
}
