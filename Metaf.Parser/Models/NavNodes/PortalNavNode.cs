﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Metaf.Parser.Models.NavNodes {
    /// <summary>
    /// A nav node that uses the specified portal id.
    /// 
    /// WARNING this nav node type is DEPRECATED and only here for compatibility reasons!
    /// Use <see cref="UsePortalNPCNavNode"/> instead!
    /// </summary>
    [Obsolete("This class is deprecated! Use UsePortalNPCNavNode instead!")]
    public class PortalNavNode : PositionNavNode {
        /// <summary>
        /// The id of the portal object to use.
        /// </summary>
        public int PortalId { get; set; }

        [Obsolete("This class is deprecated! Use UsePortalNPCNavNode instead!")]
        public PortalNavNode() : base(NavNodeType.Portal) {

        }
    }
}
