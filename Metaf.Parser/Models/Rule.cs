﻿namespace Metaf.Parser.Models {

    /// <summary>
    /// A rule contained within a state.
    /// </summary>
    public class Rule {
        /// <summary>
        /// The condition to be evaluated
        /// </summary>
        public ConditionBase Condition { get; set; }

        /// <summary>
        /// The action to take if the condition evaluates to true
        /// </summary>
        public ActionBase Action { get; set; }

        public Rule(ConditionBase condition, ActionBase action) {
            Condition = condition;
            Action = action;
        }
    }
}