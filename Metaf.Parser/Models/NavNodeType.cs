﻿namespace Metaf.Parser.Models {
    public enum NavNodeType {
        Follow = -2, // workaround = MY VALUE FOR THIS, not VTank's!
        Unassigned = -1,
        Point = 0,
        Portal = 1, // DEPRECATED Portal node (only has one set of coordinates instead of two (like "ptl" type has)
        Recall = 2,
        Pause = 3,
        Chat = 4,
        OpenVendor = 5,
        Portal_NPC = 6,
        NPCTalk = 7,
        Checkpoint = 8,
        Jump = 9
    }
}