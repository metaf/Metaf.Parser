﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Metaf.Parser.Models {
    public abstract class PositionNavNode : NavNodeBase {
        public double X { get; set; } = 0;
        public double Y { get; set; } = 0;
        public double Z { get; set; } = 0;

        public PositionNavNode(NavNodeType type) : base(type) {

        }
    }
}
