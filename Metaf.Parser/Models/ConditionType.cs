﻿namespace Metaf.Parser.Models {
    public enum ConditionType {
        Never = 0,
        Always = 1,
        All = 2,
        Any = 3,
        ChatMatch = 4,
        MainSlotsLE = 5,
        SecsInStateGE = 6,
        NavEmpty = 7,
        Death = 8,
        VendorOpen = 9,
        VendorClosed = 10,
        ItemCountLE = 11,
        ItemCountGE = 12,
        MobsInDist_Name = 13,
        MobsInDist_Priority = 14,
        NeedToBuff = 15,
        NoMobsInDist = 16,
        BlockE = 17,
        CellE = 18,
        IntoPortal = 19,
        ExitPortal = 20,
        Not = 21,
        PSecsInStateGE = 22,
        SecsOnSpellGE = 23,
        BuPercentGE = 24,
        DistToRteGE = 25,
        Expr = 26,
        //ClientDialogPopup = 27, // some type from the past? it's not in vt now.
        ChatCapture = 28
    }
}