﻿using System.Collections.Generic;

namespace Metaf.Parser.Models {
    public class NavRoute {
        /// <summary>
        /// The id of this nav route. This is only used for referencing nav routes in EmbedNav actions,
        /// not in the meta itself.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Type of nav route, this defines how the route is traversed.
        /// </summary>
        public NavRouteType Type { get; set; }

        /// <summary>
        /// List of nodes in this route
        /// </summary>
        public List<NavNodeBase> Nodes { get; set; } = new List<NavNodeBase>();

        public NavRoute(string id) {
            Id = id;
        }
    }
}