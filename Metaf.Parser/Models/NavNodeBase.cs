﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Metaf.Parser.Models {
    public abstract class NavNodeBase {
        public NavNodeType Type { get; set; }

        public NavNodeBase(NavNodeType type) {
            Type = type;
        }
    }
}
