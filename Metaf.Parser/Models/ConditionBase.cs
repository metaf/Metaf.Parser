﻿namespace Metaf.Parser.Models {
    public abstract class ConditionBase {
        /// <summary>
        /// The type of condition
        /// </summary>
        public ConditionType Type { get; }

        public ConditionBase(ConditionType type) {
            Type = type;
        }
    }
}