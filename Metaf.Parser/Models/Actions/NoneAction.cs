﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Metaf.Parser.Models.Actions {
    /// <summary>
    /// An action that does nothing.
    /// </summary>
    public class NoneAction : ActionBase {
        public NoneAction() : base(ActionType.None) {

        }
    }
}
