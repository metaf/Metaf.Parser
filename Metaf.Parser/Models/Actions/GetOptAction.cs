﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Metaf.Parser.Models.Actions {
    /// <summary>
    /// An action that gets the value of the specified vtank setting <see cref="SettingName"/> and sets the result to
    /// the expression variable with the name <see cref="VariableName"/>.
    /// </summary>
    public class GetOptAction : ActionBase {
        /// <summary>
        /// The name of the vtank setting
        /// </summary>
        public string SettingName { get; set; } = "";

        /// <summary>
        /// The name of the expression variable to set the resulting vtank setting value to.
        /// </summary>
        public string VariableName { get; set; } = "";

        public GetOptAction() : base(ActionType.GetOpt) {

        }
    }
}
