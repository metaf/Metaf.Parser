﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metaf.Parser.Models.Actions {
    public class SetWatchdogAction : ActionBase {
        /// <summary>
        /// 
        /// </summary>
        public double Distance { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public double Seconds { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string TransferState { get; set; }

        public SetWatchdogAction() : base(ActionType.SetWatchdog) {
        }
    }
}
