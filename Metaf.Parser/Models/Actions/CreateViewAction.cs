﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metaf.Parser.Models.Actions {
    public class CreateViewAction : ActionBase {
        /// <summary>
        /// The id of the view to create
        /// </summary>
        public string Id { get; set; } = "";

        /// <summary>
        /// The XML string of the view
        /// </summary>
        public string XML { get; set; } = "";

        public CreateViewAction() : base(ActionType.CreateView) {
        }
    }
}
