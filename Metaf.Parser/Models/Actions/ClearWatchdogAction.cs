﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Metaf.Parser.Models.Actions {
    /// <summary>
    /// An action that clears the watchdog set by <see cref="SetWatchdogAction"/> in this state.
    /// </summary>
    public class ClearWatchdogAction : ActionBase {
        public ClearWatchdogAction() : base(ActionType.ClearWatchdog) {

        }
    }
}
