﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Metaf.Parser.Models.Actions {
    /// <summary>
    /// An action that destroys all meta views created by this meta.
    /// </summary>
    public class DestroyAllViewsAction : ActionBase {
        public DestroyAllViewsAction() : base(ActionType.DestroyAllViews) {

        }
    }
}
