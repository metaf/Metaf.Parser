﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Metaf.Parser.Models.Actions {
    /// <summary>
    /// An action that switches to the state named <see cref="SetStateAction.Name"/>.
    /// </summary>
    public class SetStateAction : ActionBase {
        /// <summary>
        /// The name of the new state to switch to
        /// </summary>
        public string Name { get; set; }

        public SetStateAction() : base(ActionType.SetState) {

        }
    }
}
