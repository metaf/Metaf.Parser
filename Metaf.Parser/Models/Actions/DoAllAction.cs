﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metaf.Parser.Models.Actions {
    /// <summary>
    /// An action that performs a list of child actions
    /// </summary>
    internal class DoAllAction : MultiActionBase {
        public DoAllAction() : base(ActionType.DoAll) {
        }
    }
}
