﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Metaf.Parser.Models.Actions {
    /// <summary>
    /// An action that embeds the specified nav file.
    /// </summary>
    public class EmbedNavAction : ActionBase {
        /// <summary>
        /// The embedded nav id. This is used to reference a nav defined with a NAV: block.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// The name of this nav file, if any
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// An optional transformation to apply to the nav data.
        /// </summary>
        public NavTransform? Transform { get; set; }

        public class NavTransform {
            public double A { get; set; } = 1;
            public double B { get; set; } = 0;
            public double C { get; set; } = 0;
            public double D { get; set; } = 1;
            public double E { get; set; } = 0;
            public double F { get; set; } = 0;
            public double G { get; set; } = 0;
        }

        public EmbedNavAction() : base(ActionType.EmbedNav) {

        }
    }
}
