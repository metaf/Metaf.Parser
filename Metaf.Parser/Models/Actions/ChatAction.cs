﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Metaf.Parser.Models.Actions {
    /// <summary>
    /// An action that sends the specified <see cref="ChatAction.Message"/> to the chat window. This can be a 
    /// slash command or normal chat text. It acts as if you typed a message into the chat window.
    /// </summary>
    public class ChatAction : ActionBase {
        /// <summary>
        /// The message to send to chat.
        /// </summary>
        public string Message { get; set; }

        public ChatAction() : base(ActionType.Chat) {

        }
    }
}
