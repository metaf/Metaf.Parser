﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Metaf.Parser.Models.Actions {
    /// <summary>
    /// An action that runs the specified <see cref="Expression"/>
    /// </summary>
    public class DoExprAction : ActionBase {
        /// <summary>
        /// The expression to run
        /// </summary>
        public string Expression { get; set; }

        public DoExprAction() : base(ActionType.DoExpr) {

        }
    }
}
