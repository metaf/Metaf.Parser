﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Metaf.Parser.Models.Actions {
    /// <summary>
    /// An action that transitions to <see cref="NewState"/> state, placing <see cref="ReturnState"/> state on stack 
    /// (set-up for use by a later <see cref="ReturnAction"/>)
    /// </summary>
    public class CallStateAction : ActionBase {
        /// <summary>
        /// The state to transition to
        /// </summary>
        public string NewState { get; set; }

        /// <summary>
        /// The state to return to when the next <see cref="ReturnAction"/> runs
        /// </summary>
        public string ReturnState { get; set; }

        public CallStateAction() : base(ActionType.CallState) {

        }
    }
}
