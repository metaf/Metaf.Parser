﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Metaf.Parser.Models.Actions {
    /// <summary>
    /// An action that destroys the view with the specified <see cref="Id"/>
    /// </summary>
    public class DestroyViewAction : ActionBase {
        /// <summary>
        /// The id of the view to destroy.
        /// </summary>
        public string Id { get; set; } = "";
        public DestroyViewAction() : base(ActionType.DestroyView) {

        }
    }
}
