﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Metaf.Parser.Models.Actions {
    /// <summary>
    /// An action that evaluates <see cref="Expression"/> and then sends the resulting text to the chat window.
    /// </summary>
    public class ChatExprAction : ActionBase {
        /// <summary>
        /// The expression to evaluate. Its result will be sent to the chat window.
        /// </summary>
        public string Expression { get; set; }

        public ChatExprAction() : base(ActionType.ChatExpr) {

        }
    }
}
