﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Metaf.Parser.Models.Actions {
    /// <summary>
    /// An action that sets the value of the specified vtank setting <see cref="SettingName"/> with the value returned by
    /// evaluating the expression <see cref="Expression"/>
    /// </summary>
    public class SetOptAction : ActionBase {
        /// <summary>
        /// The name of the vtank setting
        /// </summary>
        public string SettingName { get; set; } = "";

        /// <summary>
        /// The expression to evaluate, where the results are set to the specified vtank setting.
        /// </summary>
        public string Expression { get; set; } = "";

        public SetOptAction() : base(ActionType.SetOpt) {

        }
    }
}
