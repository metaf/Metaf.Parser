﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Metaf.Parser.Models.Actions {
    /// <summary>
    /// An action that returns to the last state added to the stack by <see cref="CallStateAction"/>.
    /// </summary>
    public class ReturnAction : ActionBase {
        public ReturnAction() : base(ActionType.Return) {

        }
    }
}
