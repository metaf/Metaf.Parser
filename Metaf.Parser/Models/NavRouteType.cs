﻿namespace Metaf.Parser.Models {
    /// <summary>
    /// Type of nav route, this defines how the route is traversed.
    /// </summary>
    public enum NavRouteType {
        /// <summary>
        /// Traverses waypoints in the order ABC-ABC. (After last waypoint, will return to first waypoint)
        /// </summary>
        Circular = 1,

        /// <summary>
        /// Follows another Player.
        /// </summary>
        Follow,

        /// <summary>
        /// Traverses waypoints in the order ABC-CBA. (Will run back and forth from beginning to end)
        /// </summary>
        Linear,

        /// <summary>
        /// Runs a route just once. (Good for ToVendor/ToDungeon routes.)
        /// </summary>
        Once
    }
}