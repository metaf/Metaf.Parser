﻿using Metaf.Parser.Lib;
using Metaf.Parser.Models;
using Metaf.Parser.Visitors;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Metaf.Parser {
    /// <summary>
    /// A parser for metaf file thats transforms a raw .af file to a <see cref="Meta"/> class.
    /// </summary>
    public class ModelParser {
        private MetaFModelBuilderVisitor _builder;

        /// <summary>
        /// The error handler used for lexing / parsing
        /// </summary>
        public MetafErrorListener ErrorHandler => _builder.ErrorHandler;

        /// <summary>
        /// Create a new Metaf parser from the specified file path
        /// </summary>
        /// <param name="filename">The .af filepath to read</param>
        public static ModelParser FromFile(string filename) {
            return new ModelParser(File.ReadAllText(filename));
        }

        /// <summary>
        /// Create a new Metaf parser from the string contents of an .af file.
        /// </summary>
        /// <param name="contents">The string contents of a metaf .af file.</param>
        public ModelParser(string contents) {
            _builder = new MetaFModelBuilderVisitor(contents);
        }

        /// <summary>
        /// Try and parse the meta.
        /// </summary>
        /// <param name="meta">The meta, if it was able to be parsed. Null otherwise.</param>
        /// <returns>True if the meta was parsed without issues, false otherwise.</returns>
        public bool TryParse(out Meta? meta) {
            meta = _builder.Build();

            return !HasErrors();
        }

        /// <summary>
        /// Checks if any errors were encountered while parsing / building models.
        /// </summary>
        /// <returns>True if there were errors, false otherwise</returns>
        public bool HasErrors() {
            return ErrorHandler.Errors.Count > 0;
        }
    }
}
