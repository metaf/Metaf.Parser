﻿using Antlr4.Runtime;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metaf.Parser {
    public abstract class MetaFParserBase : Antlr4.Runtime.Parser {
        protected MetaFParserBase(ITokenStream input)
            : base(input) {
        }

        public bool CannotBePlusMinus() {
            return true;
        }

        public bool CannotBeDotLpEq() {
            return true;
        }
    }
}
