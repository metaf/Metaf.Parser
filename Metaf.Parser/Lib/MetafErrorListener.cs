﻿using Antlr4.Runtime.Misc;
using Antlr4.Runtime;
using Metaf.Parser.Visitors;
using System;
using System.Collections.Generic;
using System.Text;

namespace Metaf.Parser.Lib {

    public class MetafErrorListener : DefaultErrorStrategy, IAntlrErrorListener<IToken>, IAntlrErrorListener<int> {
        private MetaFParserBaseVisitor<object> _visitor;

        public class MetafError {
            public string ErrorText { get; set; } = "";
            public RecognitionException Exception { get; set; }
            public int Line { get; set; }
            public int Column { get; set; }
            public int OffendingSymbol { get; set; }
        }

        public MetafErrorListener(MetaFParserBaseVisitor<object> visitor) {
            _visitor = visitor;
        }

        public List<MetafError> Errors { get; } = new List<MetafError>();

        public void SyntaxError([NotNull] IRecognizer recognizer, [Nullable] int offendingSymbol, int line, int charPositionInLine, [NotNull] string msg, [Nullable] RecognitionException e) {
            Errors.Add(new MetafError() {
                ErrorText = msg,
                Exception = e,
                Line = line,
                Column = charPositionInLine,
                OffendingSymbol = offendingSymbol
            });
        }

        public void SyntaxError([NotNull] IRecognizer recognizer, [Nullable] IToken offendingSymbol, int line, int charPositionInLine, [NotNull] string msg, [Nullable] RecognitionException e) {
            Errors.Add(new MetafError() {
                ErrorText = msg,
                Exception = e,
                Line = line,
                Column = charPositionInLine,
                OffendingSymbol = offendingSymbol?.Type ?? -1
            });
        }
    }
}
