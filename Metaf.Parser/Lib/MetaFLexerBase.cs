﻿using Antlr4.Runtime;
using Antlr4.Runtime.Misc;
using AntlrDenter;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static AntlrDenter.DenterHelper;

namespace Metaf.Parser {
    public abstract class MetaFLexerBase : Lexer {
        private DenterHelper denter;
        private Queue<IToken> _tokenQueue = new Queue<IToken>();

        protected MetaFLexerBase(ICharStream input) : base(input) {
        }

        public override IToken NextToken() {
            if (_tokenQueue.Count > 0) {
                return _tokenQueue.Dequeue();
            }
            if (denter == null) {
                denter = Builder()
                    .Nl(MetaFLexer.NL)
                    .Indent(MetaFParser.INDENT)
                    .Dedent(MetaFParser.DEDENT)
                    .PullToken(base.NextToken);
            }

            var t = denter.NextToken();
            if (t.Type == MetaFParser.OPENCURL) {
                int _nn = -1;
                var i = 1;
                bool allNumbers = true;
                var numberTypes = new List<int>() {
                    MetaFLexer.NUMBER,
                    MetaFLexer.DECIMAL
                };
                var _t = new List<string>();
                do {
                    _nn = InputStream.La(i++);
                    if (_nn != MetaFLexer.CLOSECURL && !numberTypes.Contains(_nn)) {
                        allNumbers = false;
                        break;
                    }
                    _t.Add(MetaFLexer.DefaultVocabulary.GetSymbolicName(_nn));
                }
                while (_nn != MetaFLexer.CLOSECURL);

                if (allNumbers && i > 2) {
                    return t;
                }

                var n = NextToken();
                if (n.Type == MetaFParser.CLOSECURL) {
                    return TokenFactory.Create(MetaFParser.CURLYCONTENTS, "");
                }
                else {
                    var depth = 1;
                    var start = n.StartIndex;
                    var prev = n;
                    while (depth > 0 && t.Type >= 0) {
                        t = denter.NextToken();
                        if (!(prev.Type == MetaFParser.ANY && prev.Text == "\\")) {
                            if (t.Type == MetaFParser.OPENCURL) {
                                depth++;
                            }
                            else if (t.Type == MetaFParser.CLOSECURL) {
                                depth--;
                            }
                        }
                        prev = t;
                    }
                    var text = _input.GetText(Interval.Of(start, t.StopIndex))
                        .Replace("\\{", "{").Replace("\\}", "}");
                    text = text.Substring(0, text.Length - 1)
                        .Replace("{{", "{").Replace("}}", "}");
                    return TokenFactory.Create(MetaFParser.CURLYCONTENTS, text);
                }
            }

            return t;
        }
    }
}
