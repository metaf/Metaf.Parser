lexer grammar MetaFLexer;

options {
    superClass = MetaFLexerBase;
}
tokens { INDENT, DEDENT, CURLYCONTENTS }

STATE_LABEL: 'STATE:' ;
IF_LABEL: 'IF:' ;
DO_LABEL: 'DO:' ;
NAV_LABEL: 'NAV:' ;

NAV_TYPE_CIRCULAR: 'circular' ;
NAV_TYPE_LINEAR: 'linear' ;
NAV_TYPE_ONCE: 'once' ;
NAV_TYPE_FOLLOW: 'follow' ;

NAV_PNT: 'pnt' ;
NAV_FLW: 'flw' ;
NAV_PRT: 'prt' ;
NAV_RCL: 'rcl' ;
NAV_PAU: 'pau' ;
NAV_CHT: 'cht' ;
NAV_VND: 'vnd' ;
NAV_PTL: 'ptl' ;
NAV_TLK: 'tlk' ;
NAV_CHK: 'chk' ;
NAV_JMP: 'jmp' ;

ACTION_SET_STATE: 'SetState' ;
ACTION_CHAT: 'Chat' ;
ACTION_DO_ALL: 'DoAll' ;
ACTION_EMBED_NAV: 'EmbedNav' ;
ACTION_CALL_STATE: 'CallState' ;
ACTION_RETURN: 'Return' ;
ACTION_DO_EXPR: 'DoExpr' ;
ACTION_CHAT_EXPR: 'ChatExpr' ;
ACTION_SET_WATCHDOG: 'SetWatchdog' ;
ACTION_CLEAR_WATCHDOG: 'ClearWatchdog' ;
ACTION_GET_OPT: 'GetOpt' ;
ACTION_SET_OPT: 'SetOpt' ;
ACTION_CREATE_VIEW: 'CreateView' ;
ACTION_DESTROY_VIEW: 'DestroyView' ;
ACTION_DESTROY_ALL_VIEWS: 'DestroyAllViews' ;
ACTION_NONE: 'None' ;

IF_COND_ALL: 'All' ;
IF_COND_ALWAYS: 'Always' ;
IF_COND_ANY: 'Any' ;
IF_COND_BLOCK_E: 'BlockE' ;
IF_COND_BU_PERCENT_GE: 'BuPercentGE' ;
IF_COND_CELL_E: 'CellE' ;
IF_COND_CHAT_CAPTURE: 'ChatCapture' ;
IF_COND_CHAT_MATCH: 'ChatMatch' ;
IF_COND_DEATH: 'Death' ;
IF_COND_DIST_TO_RTE_GE: 'DistToRteGE' ;
IF_COND_EXIT_PORTAL: 'ExitPortal' ;
IF_COND_EXPR: 'Expr' ;
IF_COND_INTO_PORTAL: 'IntoPortal' ;
IF_COND_ITEM_COUNT_GE: 'ItemCountGE' ;
IF_COND_ITEM_COUNT_LE: 'ItemCountLE' ;
IF_COND_MAIN_SLOTS_LE: 'MainSlotsLE' ;
IF_COND_MOBS_IN_DIST_NAME: 'MobsInDist_Name' ;
IF_COND_MOBS_IN_DIST_PRIORITY: 'MobsInDist_Priority' ;
IF_COND_NAV_EMPTY: 'NavEmpty' ;
IF_COND_NEED_TO_BUFF: 'NeedToBuff' ;
IF_COND_NEVER: 'Never' ;
IF_COND_NO_MOBS_IN_DIST: 'NoMobsInDist' ;
IF_COND_NOT: 'Not' ;
IF_COND_P_SECS_IN_STATE_GE: 'PSecsInStateGE' ;
IF_COND_SECS_IN_STATE_GE: 'SecsInStateGE' ;
IF_COND_VENDOR_CLOSED: 'VendorClosed' ;
IF_COND_VENDOR_OPEN: 'VendorOpen' ;

fragment INDENTATION: (' ' | '\t')* ;

OPENCURL: '{' ;
CLOSECURL: '}' ;
PLUS: '+'  ;
MINUS: '-' ;
SEMICOLON: ';' ;

fragment HCHAR: [A-Fa-f0-9];
HEX: ('0x')? HCHAR HCHAR HCHAR HCHAR HCHAR HCHAR HCHAR HCHAR ;

fragment DIGIT: [0-9] ;
fragment CHAR: ~('{' | '}' | ' ' | '\t' | '\r' | '\n' | ';' | '\\' | [0-9]);
NUMBER: DIGIT+;
DECIMAL: MINUS? DIGIT+ ('.' DIGIT+)? ('E' ('+' | '-') [0-9]+)? ;
IDENTIFIER: CHAR (CHAR | DIGIT)*;

COMMENT: ((('\r' | '\n')? '~~' ~('\n')*) | ('/~' .*? '~/')) -> channel(HIDDEN);

NL: ('\r'? '\n' INDENTATION) ;

WS: (' ' | '\t')+ -> skip;
ANY: . ;

